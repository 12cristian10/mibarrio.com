/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.dao;

import com.mintic.mibarrio.models.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface ProductoDao extends JpaRepository<Producto, Integer> {
    
    public Producto findByNombreProducto(String nombreProducto);
    
    public Producto findByCodigoProducto(String codigoProducto);
   
    public List<Producto> findAllByCategoria(String categoria);
    
    public List<Producto> OrderByNombreProducto();
    
    public List<Producto> findAllByProveedor_IdProveedor(Integer idProveedor);
}
