/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.dao;

import com.mintic.mibarrio.models.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface UsuarioDao extends JpaRepository<Usuario, Integer>{
    
    public List<Usuario> findByNombresOrApellidos(String nombres, String apellidos);
    
    public Usuario findByUsuario(String usuario);
    
    public List<Usuario> findByTipoDocumento(String tipoDoc);
    
    public List<Usuario> findByRol(String rol);
     
    public Usuario findByUsuarioAndClave(String usuario, String clave);
}
