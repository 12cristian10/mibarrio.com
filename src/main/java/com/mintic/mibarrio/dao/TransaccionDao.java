/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.dao;

import com.mintic.mibarrio.models.Transaccion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface TransaccionDao extends JpaRepository<Transaccion, Integer>{
    
    public Transaccion findByCodigoTransaccion(String codigoTransaccion);
    
    public List <Transaccion> findByTipoTransaccion(String tipoTransaccion);
    
    public List<Transaccion> OrderByTotalTransaccionDesc();
    
}
