/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.controllers;

import com.mintic.mibarrio.models.Usuario;
import com.mintic.mibarrio.services.UsuarioService;
import com.mintic.mibarrio.util.PasswordHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/")
public class HomeController {
    
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping(value = "login")
    public ResponseEntity<Usuario> consultarLogin(@RequestBody Usuario usuario) {

        String claveCifrada = PasswordHash.cifrarPassword(usuario.getClave());

        Usuario resultado = usuarioService.buscarCredenciales(usuario.getUsuario(), claveCifrada);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }


}
