/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.controllers;

import com.mintic.mibarrio.models.Proveedor;
import com.mintic.mibarrio.services.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/proveedor")
public class ProveedorController { 
    
    @Autowired
    private ProveedorService proveedorService;
    
    @GetMapping(value = "/listar")
    public List<Proveedor> listarProveedores() {
        return proveedorService.listar();
    } 
    
    @GetMapping(value = "/listar/{id}")
    public Proveedor consultarPorId(@PathVariable Integer id) {
        return proveedorService.buscarId(id);
    }

    @GetMapping(value = "/listar/nombre/{nombre}")
    public Proveedor consultarPorNombre(@PathVariable String nombre) {
        return proveedorService.buscarNombre(nombre);
    } 
    
    @PostMapping(value = "/")
    public ResponseEntity<Proveedor> agregar(@RequestBody Proveedor proveedor) {
        Proveedor resultado = proveedorService.guardar(proveedor);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Proveedor> editar(@RequestBody Proveedor nuevo) {

        Proveedor actual = proveedorService.buscarId(nuevo.getIdProveedor());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setNitProveedor(nuevo.getNitProveedor());
            actual.setNombreProveedor(nuevo.getNombreProveedor());
            actual.setTelefonoProveedor(nuevo.getTelefonoProveedor());
            actual.setEmailProveedor(nuevo.getEmailProveedor());
            
            proveedorService.guardar(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Proveedor> eliminar(@PathVariable Integer id) {

        Proveedor resultado = proveedorService.buscarId(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            proveedorService.eliminar(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }


}
