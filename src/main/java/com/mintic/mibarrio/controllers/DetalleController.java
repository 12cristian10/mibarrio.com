/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.controllers;

import com.mintic.mibarrio.models.Detalle;
import com.mintic.mibarrio.models.Transaccion;
import com.mintic.mibarrio.services.DetalleService;
import com.mintic.mibarrio.services.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class DetalleController {
    
    @Autowired
    private DetalleService detalleService;
    
    @Autowired
    private TransaccionService transaccionService;
    
    @GetMapping(value = "/listar")
    public List<Detalle> listarDetalles() {
        return detalleService.listar();
    }

    @GetMapping(value = "/listar/{id}")
    public Detalle consultarPorId(@PathVariable Integer id) {
        return detalleService.buscarId(id);
    }
    
    @GetMapping(value = "/transaccion/{id}")
    public ResponseEntity<List<Detalle>> consultarPorTransaccionId(@PathVariable Integer id) {

        List<Detalle> detalles = null;
        Transaccion transaccion = transaccionService.buscarId(id);

        if (transaccion == null) {
            return new ResponseEntity<>(detalles, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            detalles = detalleService.filtrarTransaccion(transaccion.getIdTransaccion());
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        }
    } 
    
    @PostMapping(value = "/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle) {
        Detalle result = detalleService.guardar(detalle);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle nuevo) {

        Detalle actual = detalleService.buscarId(nuevo.getIdDetalle());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setProducto(nuevo.getProducto());
            actual.setTransaccion(nuevo.getTransaccion());
            actual.setCantidad(nuevo.getCantidad());
            actual.setSubtotal(nuevo.getProducto().getPrecioVenta() * nuevo.getCantidad());

            detalleService.guardar(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id) {

        Detalle resultado = detalleService.buscarId(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            detalleService.eliminar(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }

}
