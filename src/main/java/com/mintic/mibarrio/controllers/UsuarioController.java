/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.controllers;

import com.mintic.mibarrio.models.Usuario;
import com.mintic.mibarrio.services.UsuarioService;
import com.mintic.mibarrio.util.PasswordHash;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @GetMapping(value = "/listar")
    public List<Usuario> listarUsuarios() {
        return usuarioService.listar();
    }

    @GetMapping(value = "/listar/{id}")
    public Usuario consultarPorId(@PathVariable Integer id) {
        return usuarioService.buscarId(id);
    }
    
    @GetMapping(value = "/listar/{nombres}/{apellidos}")
    public List<Usuario> consultarPorNombres(@PathVariable String nombres, @PathVariable String apellidos) {
        return usuarioService.buscarNombres(nombres, apellidos);
    } 
    
    @GetMapping(value = "/listar/{usuario}")
    public Usuario consultarPorUsuario(@PathVariable String usuario) {
        return usuarioService.buscarUsuario(usuario);
    } 
    
    @GetMapping(value = "/listar/tipo/{tipo}")
    public List<Usuario> listarPorTipoDocumento(String tipo) {
        return usuarioService.filtrarTipoDocumento(tipo);
    }
    
    @GetMapping(value = "/listar/rol/{rol}")
    public List<Usuario> listarPorRol(String rol) {
        return usuarioService.filtrarRol(rol);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario) {

        
        String claveCifrada = PasswordHash.cifrarPassword(usuario.getClave());
        usuario.setClave(claveCifrada);
        Usuario result = usuarioService.guardar(usuario);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario nuevo) {

        Usuario actual = usuarioService.buscarId(nuevo.getIdUsuario());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {

            
            actual.setIdUsuario(nuevo.getIdUsuario());
            actual.setNombres(nuevo.getNombres());

            if (!nuevo.getClave().equals(actual.getClave())) {
                String claveCifrada = PasswordHash.cifrarPassword(nuevo.getClave());
                actual.setClave(claveCifrada);
            }
            
            actual.setNombres(nuevo.getNombres());
            actual.setApellidos(nuevo.getApellidos());
            actual.setRol(nuevo.getRol());
            actual.setUsuario(nuevo.getUsuario());
            actual.setDireccion(nuevo.getDireccion());
            actual.setTelefono(nuevo.getTelefono());
            actual.setEmail(nuevo.getEmail());


            usuarioService.guardar(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id) {

        Usuario resultado = usuarioService.buscarId(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            usuarioService.eliminar(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }


}
