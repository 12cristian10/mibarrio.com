/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.controllers;

import com.mintic.mibarrio.models.Producto;
import com.mintic.mibarrio.models.Proveedor;
import com.mintic.mibarrio.services.ProductoService;
import com.mintic.mibarrio.services.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")

public class ProductoController {
    
    @Autowired
    private ProductoService productoService;
    
    @Autowired
    private ProveedorService proveedorService;
    
    @GetMapping(value = "/listar")
    public List<Producto> listarProductos() {
        return productoService.listar();
    } 
    
    @GetMapping(value = "/listar/{id}")
    public Producto consultarPorId(@PathVariable Integer id) {
        return productoService.buscarId(id);
    }

    @GetMapping(value = "/listar/nombre/{nombre}")
    public Producto consultarPorNombre(@PathVariable String nombre) {
        return productoService.buscarNombre(nombre);
    }
    
    @GetMapping(value = "/listar/nombre")
    public List<Producto> ordenarPorNombre() {
        return productoService.ordenarNombre();
    }
    
    @GetMapping(value = "/listar/codigo/{codigo}")
    public Producto consultarPorCodigo(@PathVariable String codigo) {
        return productoService.buscarCodigo(codigo);
    } 
    
    @GetMapping(value = "/listar/categoria/{categoria}")
    public List<Producto> consultarPorCategoria(@PathVariable String categoria) {
        return productoService.filtrarCategoria(categoria);
    }
    
    @GetMapping(value = "/proveedor/{id}")
    public ResponseEntity<List<Producto>> consultarPorProveedorId(@PathVariable Integer id) {

        List<Producto> productos = null;
        Proveedor proveedor = proveedorService.buscarId(id);

        if (proveedor == null) {
            return new ResponseEntity<>(productos, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            productos = productoService.filtrarProveedor(proveedor.getIdProveedor());
            return new ResponseEntity<>(productos, HttpStatus.OK);
        }
    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto) {
        Producto resultado = productoService.guardar(producto);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Producto> editar(@RequestBody Producto nuevo) {

        Producto actual = productoService.buscarId(nuevo.getIdProducto());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setCodigoProducto(nuevo.getCodigoProducto());
            actual.setNombreProducto(nuevo.getNombreProducto());
            actual.setCategoria(nuevo.getCategoria());
            actual.setPrecioCompra(nuevo.getPrecioCompra());
            actual.setPrecioVenta(nuevo.getPrecioVenta());
            actual.setDescripcion(nuevo.getDescripcion());
            actual.setProveedor(nuevo.getProveedor());
            
            productoService.guardar(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id) {

        Producto resultado = productoService.buscarId(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            productoService.eliminar(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
