/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.services;

import com.mintic.mibarrio.models.Transaccion;
import java.util.List;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface TransaccionService {
    
    public Transaccion guardar(Transaccion transaccion);
  
    public void eliminar(Integer id);
    
    public Transaccion buscarId(Integer id);
    
    public Transaccion buscarCodigo(String codigoTransaccion);
    
    public List<Transaccion> listar();
    
    public List<Transaccion> filtrarTipo(String TipoTransaccion);
    
    public List<Transaccion> ordenarTotal();   
    
}
