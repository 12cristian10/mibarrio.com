/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.services.Impl;

import com.mintic.mibarrio.dao.DetalleDao;
import com.mintic.mibarrio.models.Detalle;
import com.mintic.mibarrio.services.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Service
public class DetalleServiceImpl implements DetalleService{
    
    @Autowired
    private DetalleDao detalleDao;

    @Override
    @Transactional (readOnly = false)
    public Detalle guardar(Detalle detalle) {
        return detalleDao.save(detalle);
    }

    @Override
    @Transactional (readOnly = false)
    public void eliminar(Integer id) {
        detalleDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Detalle buscarId(Integer id) {
        return detalleDao.findById(id).orElse(null);
        
    }

    @Override
    @Transactional (readOnly = true)
    public List<Detalle> listar() {
        return (List<Detalle>) detalleDao.findAll();
    }

    @Override
    public List<Detalle> filtrarTransaccion(Integer id) {
        return detalleDao.findAllByTransaccion_IdTransaccion(id);
    }

}
