/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.services.Impl;

import com.mintic.mibarrio.dao.ProductoDao;
import com.mintic.mibarrio.models.Producto;
import com.mintic.mibarrio.services.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Service
public class ProductoServiceImpl implements ProductoService{
    
    @Autowired
    private ProductoDao productoDao;
    
    @Override
    @Transactional (readOnly = false)
    public Producto guardar(Producto producto) {
        return productoDao.save(producto);
    }

    @Override
    @Transactional (readOnly = false)
    public void eliminar(Integer id) {
        productoDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Producto buscarId(Integer id) {
        return productoDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional (readOnly = true)
    public Producto buscarNombre(String nombre) {
        return productoDao.findByNombreProducto(nombre);
    } 
    
    @Override
    @Transactional (readOnly = true)
    public Producto buscarCodigo(String codigo) {
        return productoDao.findByCodigoProducto(codigo);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Producto> listar() {
        return (List<Producto>) productoDao.findAll();
    }

    @Override
    @Transactional (readOnly = true)
    public List<Producto> filtrarCategoria(String categoria) {
       return productoDao.findAllByCategoria(categoria);
    }
    
    @Override
    @Transactional (readOnly = true)
    public List<Producto> filtrarProveedor(Integer id) {
        return productoDao.findAllByProveedor_IdProveedor(id);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Producto> ordenarNombre() {
        return productoDao.OrderByNombreProducto();
    }

}
