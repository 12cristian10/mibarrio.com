/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.services.Impl;

import com.mintic.mibarrio.dao.TransaccionDao;
import com.mintic.mibarrio.models.Transaccion;
import com.mintic.mibarrio.services.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Service
public class TransaccionServiceImpl implements TransaccionService{
    
    @Autowired
    private TransaccionDao transaccionDao;

    @Override
    @Transactional (readOnly = false)
    public Transaccion guardar(Transaccion transaccion) {
        return transaccionDao.save(transaccion);
    }

    @Override
    @Transactional (readOnly = false)
    public void eliminar(Integer id) {
        transaccionDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Transaccion buscarId(Integer id) {
        return transaccionDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public Transaccion buscarCodigo(String codigoTransaccion) {
        return transaccionDao.findByCodigoTransaccion(codigoTransaccion);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Transaccion> listar() {
        return (List<Transaccion>) transaccionDao.findAll();
    }

    @Override
    @Transactional (readOnly = true)
    public List<Transaccion> filtrarTipo(String TipoTransaccion) {
        return transaccionDao.findByTipoTransaccion(TipoTransaccion);
    }


    @Override
    @Transactional (readOnly = true)
    public List<Transaccion> ordenarTotal() {
        return transaccionDao.OrderByTotalTransaccionDesc();
    }

}
