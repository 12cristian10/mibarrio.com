/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.services.Impl;

import com.mintic.mibarrio.dao.UsuarioDao;
import com.mintic.mibarrio.models.Usuario;
import com.mintic.mibarrio.services.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Service
public class UsuarioServiceImpl implements UsuarioService{
    
    @Autowired
    private UsuarioDao usuarioDao;
    

    @Override
    @Transactional (readOnly = false)
    public Usuario guardar(Usuario usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    @Transactional (readOnly = false)
    public void eliminar(Integer id) {
       usuarioDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Usuario buscarId(Integer id) {
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Usuario> buscarNombres(String nombres, String apellidos) {
        return usuarioDao.findByNombresOrApellidos(nombres, apellidos);
    } 
    
    @Override
    @Transactional (readOnly = true)
    public Usuario buscarUsuario(String usuario) {
        return usuarioDao.findByUsuario(usuario);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Usuario> listar() {
        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    @Transactional (readOnly = true)
    public List<Usuario> filtrarTipoDocumento(String tipo) {
        return usuarioDao.findByTipoDocumento(tipo);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Usuario> filtrarRol(String rol) {
        return usuarioDao.findByRol(rol);
    }

    @Override
    public Usuario buscarCredenciales(String usuario, String clave) {
        return usuarioDao.findByUsuarioAndClave(usuario, clave);
    }   

}
