/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.services.Impl;

import com.mintic.mibarrio.dao.ProveedorDao;
import com.mintic.mibarrio.models.Proveedor;
import com.mintic.mibarrio.services.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Service
public class ProveedorServiceImpl implements ProveedorService{
    
    @Autowired
    private ProveedorDao proveedorDao;

    @Override
    @Transactional (readOnly = false)
    public Proveedor guardar(Proveedor proveedor) {
        return proveedorDao.save(proveedor);
    }

    @Override
    @Transactional (readOnly = false)
    public void eliminar(Integer id) {
        proveedorDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Proveedor buscarId(Integer id) {
        return proveedorDao.findById(id).orElse(null);
    }
    
    
    @Override
    @Transactional (readOnly = true)
    public Proveedor buscarNombre(String nombre) {
        return proveedorDao.findByNombreProveedor(nombre);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Proveedor> listar() {
        return (List<Proveedor>) proveedorDao.findAll();
    }

}
