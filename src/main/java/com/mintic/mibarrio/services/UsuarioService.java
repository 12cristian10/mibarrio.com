/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.services;

import com.mintic.mibarrio.models.Usuario;
import java.util.List;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface UsuarioService {
    
        
    public Usuario guardar(Usuario usuario);
    
    public void eliminar(Integer id);
    
    public Usuario buscarId(Integer id);
    
    public List<Usuario> buscarNombres(String nombres, String apellidos);
    
    public Usuario buscarUsuario(String usuario);
    
    public List<Usuario> listar();
    
    public List<Usuario> filtrarTipoDocumento(String tipo);
    
    public List<Usuario> filtrarRol(String rol);
    
    public Usuario buscarCredenciales(String usuario, String clave);
    
}
