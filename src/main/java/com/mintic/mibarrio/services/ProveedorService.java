/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.services;

import com.mintic.mibarrio.models.Proveedor;
import java.util.List;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface ProveedorService {
    
    public Proveedor guardar(Proveedor proveedor);
    
    public void eliminar(Integer id);
    
    public Proveedor buscarId(Integer id);
    
    public Proveedor buscarNombre(String nombre);
    
    public List<Proveedor> listar();
}
