/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.services;

import com.mintic.mibarrio.models.Detalle;
import java.util.List;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface DetalleService {
    
    public Detalle guardar(Detalle detalle);
    
    public void eliminar(Integer id);
    
    public Detalle buscarId(Integer id);
    
    public List<Detalle> listar();
    
    public List<Detalle> filtrarTransaccion(Integer id);
    
}
