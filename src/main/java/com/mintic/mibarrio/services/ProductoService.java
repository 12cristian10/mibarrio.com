/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.mibarrio.services;

import com.mintic.mibarrio.models.Producto;
import java.util.List;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public interface ProductoService {
    
    public Producto guardar(Producto producto);
    
    public void eliminar(Integer id);
    
    public Producto buscarId(Integer id);
    
    public Producto buscarNombre(String nombre);
    
    public Producto buscarCodigo(String codigo);
    
    public List<Producto> listar();
    
    public List<Producto> filtrarCategoria(String categoria);
    
    public List<Producto> filtrarProveedor(Integer id);
    
    public List<Producto> ordenarNombre();   
    
}
