/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Getter 
@Setter
@Table
@Entity (name="producto")

public class Producto implements Serializable {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    @Column (name = "idProducto")
    private Integer idProducto;
    
    @Column (name = "codigoProducto")
    private String codigoProducto;
    
    @Column (name = "nombreProducto")
    private String nombreProducto;
    
    @Column (name = "categoria")
    private String categoria;
    
    @Column (name = "precioCompra")
    private double precioCompra;
    
    @Column (name = "precioVenta")
    private double precioVenta;
    
    @Column (name = "descripcion")
    private String descripcion;
    
    @ManyToOne
    @JoinColumn(name="idProveedor")
    private Proveedor proveedor; 

}
