/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Getter 
@Setter
@Table
@Entity (name="usuario")

public class Usuario implements Serializable { 
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)  
    @Column (name = "idUsuario")
    private Integer idUsuario;
    
    @Column (name = "nombres")
    private String nombres;
    
    @Column (name = "apellidos")
    private String apellidos;
    
    @Column (name = "usuario")
    private String usuario;
    
    @Column (name = "clave")
    private String clave;
    
    @Column (name = "tipoDocumento")
    private String tipoDocumento;
    
    @Column (name = "numeroDocumento")
    private String numeroDocumento;
    
    @Column (name = "rol")
    private String rol;
    
    @Column (name = "email")
    private String email;
    
    @Column (name = "telefono")
    private String telefono;
    
    @Column (name = "direccion")
    private String direccion;
    
}
