/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

@Getter 
@Setter
@Table
@Entity (name="detalle")

public class Detalle implements Serializable { 
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)  
    @Column (name = "idDetalle")
    private Integer idDetalle;
    
    @Column (name = "cantidad")
    private Integer cantidad;
    
    @Column (name = "subtotal")
    private double subtotal;
    
    @ManyToOne
    @JoinColumn(name="idProducto")
    private Producto producto;
    
    @ManyToOne
    @JoinColumn(name="idTransaccion")
    private Transaccion transaccion;
    

}
