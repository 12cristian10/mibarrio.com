/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */


@Getter 
@Setter
@Table
@Entity (name="proveedor")

public class Proveedor implements Serializable { 
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)  
    @Column (name = "idProveedor")
    private Integer idProveedor;
    
    @Column (name = "nitProveedor")
    private String nitProveedor;
    
    @Column (name = "nombreProveedor")
    private String nombreProveedor;
    
    @Column (name = "telefonoProveedor")
    private String telefonoProveedor;
    
    @Column (name = "emailProveedor")
    private String emailProveedor;

}
