/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mintic.mibarrio.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */


@Getter 
@Setter
@Table
@Entity (name="transaccion")

public class Transaccion implements Serializable { 
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "idTransaccion")
    private Integer idTransaccion;
    
    @Column (name = "codigoTransaccion")
    private String codigoTransaccion;
    
    @Column (name = "fechaTransaccion")
    private String fechaTransaccion;
    
    @Column (name = "totalTransaccion")
    private double totalTransaccion;
    
    @Column (name = "tipoTransaccion")
    private String tipoTransaccion;
    
    @ManyToOne
    @JoinColumn(name="idUsuario")
    private Usuario usuario;
    
}
